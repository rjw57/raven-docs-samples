# Simple G Suite@Cambridge Proxy

This container provides a basic Apache configuration which can be used to proxy
another container and protect resources by requiring a valid sign in to
G Suite@Cambridge.

## Pre-requisites

You will need to register your application with Google. These steps are
well-documented on the web and Google themselves provide
[documentation](https://developers.google.com/identity/sign-in/web/sign-in) on
how to register an OAuth2 client ID and secret.

You will also need to select a "redirect URI". This is a URI on your server
which should not exist and will instead be claimed by the proxy to receive the
authentication response from Google. A good choice is something like
"https://your-server-hostname/.oidc/redirect_uri" but it doesn't really matter
what it is as long as it doesn't correspond to an existing URL for your
application.

> This redirect URI must match the one set in your OAuth2 application
> configuration in the Google developers' control panel.

## Example

This example will create a simple proxy at http://localhost:8000/ to the
[whoami](https://github.com/containous/whoami) server which can be used to debug
web requests. The whoami server simply renders the hostname of the server along
with all the HTTP headers sent by your browser.

You should create some OAuth2 web application credentials with the following
settings:

* Authorised redirect URL: http://localhost:8000/.oidc/redirect_uri

Record the application client id and secret in the ``CLIENT_ID`` and
``CLIENT_SECRET`` environment variables.

```console
# Prefix these commands with " " to avoid them being recorded in the history.
$  export CLIENT_ID=ABCDEFGHIJKLMN  # replace with value from Google
$  export CLIENT_SECRET=1234567     # replace with value from Google
```

Set the ``REDIRECT_URI`` to your application's redirect URI:

```console
$ export REDIRECT_URI=http://localhost:8000/.oidc/redirect_uri
```

Finally we need to set some passphrase used to encrypt session cookies:

```console
# Prefix these commands with " " to avoid them being recorded in the history.
$  export OIDC_CRYPTO_PASSPHRASE=some-strong-passphrase
```

Start the ``whoami`` server:

```console
$ docker run --rm --detach --name whoami containous/whoami
```

Note that we have not exposed any ports. At the moment you can't point your
browser at the ``whoami`` server. We'll expose the server using the proxy:

```console
$ docker run --rm -it \
    -e SERVER_NAME=localhost \
    -e BACKEND_URL=http://whoami/ --link whoami \
    -e CLIENT_ID -e CLIENT_SECRET -e REDIRECT_URI -e OIDC_CRYPTO_PASSPHRASE \
    -p 8000:80 \
    uisautomation/ucam-gsuite-proxy
```

Visiting http://localhost:8000/ results in a protected instance of ``whoami``.
Note that various user profile values are passed in HTTP headers.

## Configuration

The following environment variables are used for configuration:

* BACKEND_URL (required) - URL of site to proxy. Example: http://example.com/
* SERVER_NAME (required) - FQDN of site.
* REDIRECT_URI (optional) - URI of OpenID connect redirect. Claimed by proxy.
* CLIENT_ID (required) - Google OAuth2 client id.
* CLIENT_SECRET (required) - Google OAuth2 client secret.
* OIDC_CRYPTO_PASSPHRASE (required) - Strong passphrase used to encrypt session
    cookies.
* REQUIRED_DOMAIN (optional) - Google hosted domain to restrict access to.
    Default: ``cam.ac.uk``.
